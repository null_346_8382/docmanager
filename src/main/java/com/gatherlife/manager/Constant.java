package com.gatherlife.manager;

public class Constant {
	
	public static final String	DES_KEY						= "gatherlife.cn";

	// cookie生命周期,以秒为单位。参数为负数代表关闭浏览器时清除cookie,参数为0时代表删除cookie,参数为正数时代表cookie存在多少秒。
	public static final int		COOKIE_MAXAGE				= 60 * 60 * 24 * 365;
	public static final int		CLIENT_COOKIE_MAXAGE		= 60 * 60 * 24;
	// 设置路径,只有设置该cookie路径及其子路径可以访问
	public static final String	COOKIE_PATH					= "/";

	public static final String	PLATFORM_COOKIE_NAME_TOKEN	= "ptoken";
	public static final String	SYSTEM_COOKIE_NAME_TOKEN	= "ptoken";

	public static final String	USER_DOMAIN					= "docmanager.gatherlife.cn";
	public static final String	SYS_DOMAIN					= "docmanager.gatherlife.cn";

	// redis中的令牌过期时间
	public static final long		REDIS_COOKIE_TOKEN_EXPIRE	= 60 * 60 * 24 * 30;				// 1小时

	public static final String	PLATFORM_USER_TOKEN_PREFIX	= "PS:";
	public static final String	SYSTEM_USER_TOKEN_PREFIX	= "PS:";
	
	public final static String	TOKEN_NAME			        = "ptoken";

	public final static long		IMG_VERIFY_EFFECTIVE_TIME	= 60 * 10;
	public final static String	IMG_VERIFY_CODE_KEY			= "IV";

	// 认证失败
	public static final int		ERROR_CODE_AUTH_FAILED		= -3;

	public static String		urlScheme					= "http";

	public static String		base_upload_path			= "D:/webSite/doc-manager/";
}
