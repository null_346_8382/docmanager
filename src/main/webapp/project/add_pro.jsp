<%@ page language="java" import="java.util.*" pageEncoding="utf-8" isELIgnored="false"%>
<!DOCTYPE html>
<%@ include file="../common/comm_css.jsp"%>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<title>新增项目</title>
		<meta name="keywords" content="">
		<meta name="description" content="">
	</head>

	<body class="gray-bg">
		<div class="wrapper wrapper-content animated">
			<div class="row">
				<div class="col-sm-12">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<h5>新增项目</small></h5>
						</div>
						<div class="ibox-content">
							<form class="form-horizontal">
								<div class="form-group">
									<label class="col-sm-2 control-label">项目名称</label>

									<div class="col-sm-6">
										<input type="text" class="form-control" id="project_name">
									</div>
								</div>
								<div class="hr-line-dashed"></div>
								<div class="form-group">
									<label class="col-sm-2 control-label">项目描述</label>

									<div class="col-sm-6">
										<textarea class="form-control" rows="4" style="resize: none;" id="project_remark"></textarea>
									</div>
								</div>
								<div class="hr-line-dashed"></div>
								<div class="form-group">
									<label class="col-sm-2 control-label">接口说明</label>

									<div class="col-sm-6">
										<textarea class="form-control" rows="4" style="resize: none;" id="in_explain"></textarea>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="mar-t-20" style="text-align: center;">
				<button type="button" class="btn btn-w-m btn-info btn-lg" id="submit">提交</button>
				&nbsp;&nbsp;&nbsp;&nbsp;
				<button type="button" class="btn btn-w-m btn-info btn-lg" id="exit">返回</button>
			</div>
		</div>
	</body>
</html>
<script type="text/javascript">
	
	$("#submit").click(function(){
		var xco = new XCO();
		
		var project_name = $("#project_name").val();
		if(project_name){
			xco.setStringValue("project_name",project_name);
		}else{
			layer.msg("请输入项目名称", {time: times, icon:no});
			return;
		}
		
		var project_remark = $("#project_remark").val();
		if(project_remark){
			xco.setStringValue("project_remark",project_remark);
		}else{
			layer.msg("请输入项目描述", {time: times, icon:no});
			return;
		}
		
		var in_explain = $("#in_explain").val();
		if(in_explain){
			xco.setStringValue("in_explain",in_explain);
		}else{
			layer.msg("请输入接口说明", {time: times, icon:no});
			return;
		}
		
		var options = {
			url : "/manager/addPro.xco",
			data : xco,
			success : addProCallBack
		};
		layer.confirm('是否添加该项目？', {
			title:'提示',
		  	btn: ['是的','不要'] //按钮
		}, function(){
			$.doXcoRequest(options);
		})
	});
	
	function addProCallBack(data){
		if(data.getCode()!=0){
			layer.msg(data.getMessage(), {time: times, icon:no});
		}else{
			addModule(data.getLongValue("id"));
		}
	}
	
	function addModule(pro_id){
		var xco = new XCO();
		
		xco.setLongValue("pro_id",pro_id);
		xco.setStringValue("module_name",$("#project_name").val());
		
		var options = {
			url : "/manager/addModule.xco",
			data : xco,
			success : addModuleCallBack
		};
		$.doXcoRequest(options);
	}
	
	function addModuleCallBack(data){
		if(data.getCode()!=0){
			layer.msg(data.getMessage(), {time: times, icon:no});
		}else{
			layer.msg("添加成功", {time: times, icon:ok});
			location.href="../project/prolist.jsp";
		}
	}
	
	$("#exit").click(function(){
		location.href = '../project/prolist.jsp';
	})
</script>